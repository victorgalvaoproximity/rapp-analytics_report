#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.dbm_config as config
import csv
import logging
import logging.handlers
import os
import sys
import time
import traceback
import importlib

from authentication import Authentication
from datetime import datetime, date
from abstract_process import AbstractProcess
from sanitizer import Sanitizer
from urllib.request import urlopen


class DbmProcess(AbstractProcess):
    def __init__(self):
        super(DbmProcess, self).__init__("DBM")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "DBM",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        :type local_folder: str
        """
        self.__setup_logger()

        logging.debug("[DBM] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        # sys.setdefaultencoding('utf-8')
        logging.debug("[DBM] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def __get_file_url(self, service, query_id):
        """
        Retorna a URL de download do CSV de acordo com o report informado, executa o report caso não tenha sido gerado
        um arquivo novo no dia
        :type service: Resource
        :type query_id: long
        """
        queries = service.queries()
        # list_querys = queries.listqueries().execute()
        execute_query = queries.getquery(queryId=query_id).execute()

        latest_report_run_time = datetime.fromtimestamp(
            int(execute_query[u'metadata'][u'latestReportRunTimeMs'])/1000.0
        )
        if (datetime.now().date() - latest_report_run_time.date()).days > 0:
            queries.runquery(
                queryId=query_id,
                body={
                    'dataRange': execute_query[u'metadata'][u'dataRange']
                }
            ).execute()
            execute_query = queries.getquery(queryId=query_id).execute()
            while execute_query[u'metadata'][u'running']:
                time.sleep(5)
                execute_query = queries.getquery(queryId=query_id).execute()
        file_url = execute_query[u'metadata'][u'googleCloudStoragePathForLatestReport']
        return file_url

    def dbm_process(
            self,
            query_id,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        :type query_id: int
        :type report_config: list
        """
        self.__setup()

        logging.debug(
            "[DBM] Inicio do processo: QUERY_ID: {0}".format(
                query_id
            )
        )

        logging.debug("Aquisição de token para requests iniciada")

        service = Authentication().get_service_from_source("DBM")

        logging.debug("Token obtido (verifique o arquivo .dat gerado pelo storage de keys da API)")
        for reports in filter(lambda r: r.enabled, report_config):
            logging.debug("Obtendo URL do arquivo gerado pela execução do Report (ReportBuilder)")

            file_url = self.__get_file_url(
                service,
                query_id
            )

            logging.debug("URL de arquivo obtida: {0}".format(file_url))

            logging.debug("Formatando o nome do arquivo local")

            file_name = "{0}_{1}.csv".format(
                reports.table_file,
                datetime.now().strftime("%Y%m%d%H%M%S%f")
            )

            logging.debug("Formatação concluida: {0}".format(file_name))

            logging.debug("Formatando caminho completo local")

            csv_path = "{0}\\{1}".format(
                os.getcwd(),
                reports.local_folder
            )

            self.__setup_folder(csv_path)

            csv_path = "{0}\\{1}".format(
                csv_path,
                file_name
            )

            logging.debug("Formatação concluida: {0}".format(csv_path))

            logging.debug("Recuperando conteúdo do arquivo CSV gerado pelo Report")

            response_media = urlopen(file_url)
            response_media = response_media.read()

            logging.debug("Conteúdo recuperado, iniciando processo de normalização e parsing...")

            response_media = response_media.decode("utf-8")

            response_media = Sanitizer.normalize_response_media(
                response_media=response_media,
                need_sql_adjustment=True
            )

            logging.debug("Normalização efetuada")

            logging.debug("Abrindo para escrita o arquivo CSV local")

            with open(csv_path, 'w', newline='', encoding='utf-8') as csv_file:
                csv_writer = csv.writer(
                    csv_file,
                    delimiter=";",
                    dialect="excel"
                )
                if reports.foot_size > 0:
                    for line in response_media[:-reports.foot_size]:  # cleaning footer
                        csv_writer.writerow(line.split(";"))
                else:
                    for line in response_media[:]:
                        csv_writer.writerow(line.split(";"))

                logging.debug("Processo de escrita finalizado")

            csv_file.close()
            logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        process = DbmProcess()
        for report in config.projects["DBM"]:
            query_id = report["query_id"]
            report_config = report["cfg"]
            try:
                process.dbm_process(
                    query_id,
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["DBM"], bulk_insert=True, field_terminator=";"):
        super(DbmProcess, self).mssql_store(project=project)


if __name__ == "__main__":
    process = DbmProcess()
    for report in config.projects["DBM"]:
        query_id = report["query_id"]
        report_config = report["cfg"]
        try:
            process.dbm_process(
                query_id,
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print(msg)

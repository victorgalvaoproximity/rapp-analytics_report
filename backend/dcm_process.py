#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.dcm_config as config
import unicodecsv as csv
import json
import logging
import logging.handlers
import os
import sys
import time
import traceback
import importlib

from abstract_process import AbstractProcess
from authentication import Authentication
from datetime import datetime, date
from dateutil import parser
from sanitizer import Sanitizer
from six import string_types


class DcmProcess(AbstractProcess):
    def __init__(self):
        super(DcmProcess, self).__init__("DCM")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "DCM",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        :type local_folder: str
        """
        self.__setup_logger()

        logging.debug("[DCM] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        logging.debug("[DCM] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def __cast_by_schema_config(self, index, obj_to_cast, schema):
        """
        Converte um objeto para o tipo determinado no schema do bigquery.
        :type index: int
        :type obj_to_cast:
        :type schema: list
        :rtype:
        :raise ValueError:
        """
        if index >= len(schema):
            return obj_to_cast

        cast_type = schema[index]["type"]

        if cast_type == "INTEGER":
            casted_obj = int(obj_to_cast)
        elif cast_type == "TIMESTAMP":
            casted_obj = datetime.strptime(obj_to_cast, "%Y-%m-%d")
        elif cast_type == "FLOAT":
            casted_obj = float(obj_to_cast)
        elif cast_type == "STRING":
            casted_obj = obj_to_cast
        else:
            raise ValueError("Tipo de coluna do bigquery não mapeada: {0}".format(type(obj_to_cast)))

        return casted_obj

    def __get_custom_dcm_activities(self, line_with_columns, schema):
        """
        Encontra "activities" incluidas no ReportBuilder
        :type line_with_columns: str
        :type schema: str
        :rtype: list
        """
        columns = []
        json_columns = []

        if len(line_with_columns) > 0 and isinstance(line_with_columns[0], string_types):
            columns = line_with_columns[0].split(",")

        for i, column in enumerate(columns):
            if not any(item_schema["name"] == column for item_schema in schema):
                sanitized = column  # Sanitizer.sanitize_string(column)
                json_columns.append(dict(index=i, column_name=sanitized))

        return json_columns

    def __get_dcm_file_id(self, service, report_id, profile_id):
        """
        Encontra o o file ID de um arquivo gerado pelo ReportBuilder, se este ainda não existir ou estiver
          desatualizado, inicia o processo de construçãode um novo
        :type service: Resource
        :type report_id: int
        :type profile_id: int
        :rtype: list
        """

        """
        report_list = service.reports().list(
            profileId=profile_id
        ).execute()

        report_get = service.reports().get(
            profileId=profile_id,
            reportId=107762464
        ).execute()
        """

        last_report = service.reports().files().list(
            profileId=profile_id,
            reportId=report_id,
            maxResults=1,
            sortField="LAST_MODIFIED_TIME",
            sortOrder="DESCENDING"
        ).execute()

        need_run_report = True
        file_id = 0

        if last_report is not None and len(last_report[u"items"]) > 0:
            s, ms = divmod(int(last_report[u"items"][0][u"lastModifiedTime"]), 1000)
            date_last_file = parser.parse(
                '{}.{:03d}'.format(
                    time.strftime(
                        '%Y-%m-%d %H:%M:%S',
                        time.gmtime(s)
                    ),
                    ms
                )
            )

            file_id = last_report[u"items"][0][u"id"]
            report_type = "" if last_report[u"items"][0].get(u"format") is None else last_report[u"items"][0][u"format"]
            need_run_report = (datetime.now().date() - date_last_file.date()).days > 0 or report_type == u"EXCEL"

        if need_run_report:
            last_report = service.reports().run(
                profileId=profile_id,
                reportId=report_id
            ).execute()
            file_id = last_report[u"id"]

        request_file = service.reports().files().get(
            profileId=profile_id,
            reportId=report_id,
            fileId=file_id
        ).execute()

        if request_file.get("format") and request_file[u"format"] == u"EXCEL":
            raise ValueError("""o report foi configurado para gerar um EXCEL, o processo aceita
            apenas CSV! Modifique em: https://ddm.google.com/analytics/dfa/""")

        while request_file[u"status"] != u'REPORT_AVAILABLE':
            time.sleep(5)
            request_file = service.reports().files().get(
                profileId=profile_id,
                reportId=report_id,
                fileId=file_id
            ).execute()

        return file_id

    def __iterate_result_lines_generator(self, response_media, json_columns, skip_header_rows, schema):
        """
        Cria linhas prontas para gerar o CSV que será enviado ao CloudStorage
        :type response_media: list
        :type skip_header_rows: int
        :type schema: list
        :rtype: list
        """
        for rows in response_media[skip_header_rows + 1: -2]:  # footer contains "Grand Total" plus one blank line
            values = rows.split(",")
            string_value = ""
            json_dict = dict()
            temp_json_columns = list(json_columns)
            for i, value in enumerate(values):
                item_found = list(filter(lambda json_column: json_column['index'] == i, temp_json_columns))

                if len(item_found) is 0:
                    string_value += "{0},".format(
                        "" if value == "(not set)" else self.__cast_by_schema_config(i, value, schema)
                    )
                else:
                    json_dict[item_found[0]["column_name"]] = float(value)

                    idx_to_remove = temp_json_columns.index(item_found[0])
                    temp_json_columns.pop(idx_to_remove)

            custom_fields = json.dumps(json_dict)
            yield string_value + custom_fields

    def dcm_process(
            self,
            profile_id,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        :type profile_id: int
        :type report_config: list
        """
        self.__setup()

        logging.debug("[DCM] Inicio do processo: PROFILE_ID: {0}".format(profile_id))

        logging.debug("Aquisição de token para requests iniciada")

        service = Authentication().get_service_from_source("DCM")

        logging.debug("Token obtido (verifique o arquivo .dat gerado pelo storage de keys da API)")

        for reports in filter(lambda r: r.enabled, report_config):
            report_id = reports.report_id

            logging.debug("Obtendo id do arquivo gerado pela execução do Report. Report ID: {0}".format(report_id))

            file_id = self.__get_dcm_file_id(
                service,
                report_id,
                profile_id
            )

            logging.debug("Id de arquivo obtido: {0}".format(file_id))

            table_file = reports.table_file

            logging.debug("Formatando o nome do arquivo local")

            file_name = "{0}_{1}.csv".format(
                table_file,
                datetime.now().strftime("%Y%m%d%H%M%S%f")
            )

            logging.debug("Formatação concluida: {0}".format(file_name))

            logging.debug("Formatando caminho completo local")

            csv_path = "{0}\\{1}".format(
                os.getcwd(),
                reports.local_folder
            )

            self.__setup_folder(csv_path)

            csv_path = "{0}\\{1}".format(
                csv_path,
                file_name
            )

            logging.debug("Formatação concluida: {0}".format(csv_path))

            logging.debug("Recuperando conteúdo do arquivo CSV gerado pelo Report")

            response_media = service.files().get_media(
                reportId=report_id,
                fileId=file_id
            ).execute()

            logging.debug("Conteúdo recuperado, iniciando processo de normalização e parsing...")

            response_media = Sanitizer.normalize_response_media(
                response_media=str(response_media),
                need_fix_columns=True,
                split_line='\\n'
            )

            logging.debug("Normalização efetuada")

            logging.debug("Abrindo para escrita o arquivo CSV local")

            with open(csv_path, 'wb') as csv_file:
                csv_writer = csv.writer(
                    csv_file,
                    delimiter=",",
                    dialect="excel"
                )

                skip_rows = reports.skip_rows_csv_process
                line_with_columns = response_media[skip_rows:skip_rows + 1]

                logging.debug("Recuperando activities configuradas no report (ReportBuilder)")

                json_columns = self.__get_custom_dcm_activities(line_with_columns, reports.schema)

                logging.debug("Activities recuperadas")

                logging.debug("Criando iterador de linhas para o conteúdo do CSV gerado pelo report (ReportBuilder)")

                line_iterator = self.__iterate_result_lines_generator(
                    response_media,
                    json_columns,
                    skip_rows,
                    reports.schema
                )

                logging.debug("Iterador criado")

                logging.debug("Iniciando processo de iteração e escrita de linhas no arquivo local")

                for line in line_iterator:
                    csv_writer.writerow(line.split(","))

                logging.debug("Processo de escrita finalizado")

            csv_file.close()
            logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")
            """
            read_file = open(csv_path)

            if len(read_file.readlines()) <= 0:
                logging.debug("Arquivo CSV vazio! Arquivo será excluído, não será feito o upload!")
                os.remove(csv_path)

            read_file.close()
            """

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        for report in config.projects["DCM"]:
            profile_id = report["profile_id"]
            report_config = report["cfg"]
            try:
                self.dcm_process(
                    profile_id,
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["DCM"], bulk_insert=True, field_terminator=","):
        super(DcmProcess, self).mssql_store(project=project, field_terminator=field_terminator)


if __name__ == "__main__":
    process = DcmProcess()
    for report in config.projects["DCM"]:
        profile_id = report["profile_id"]
        report_config = report["cfg"]
        try:
            process.dcm_process(
                profile_id,
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print(msg)

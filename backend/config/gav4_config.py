#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GaV4Config:
    def __init__(
            self,
            profile_id,
            request_body,
            page_size,
            report_name="",
            days_to_load=7,
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.profile_id = profile_id
        self.request_body = request_body
        self.page_size = page_size
        self.report_name = report_name
        self.days_to_load = days_to_load
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = days_to_load
        self.code_page = sql_server_load_config.code_page


# Profiles for views
VIEW = "1"

PAGE_SIZE = 10000  # Máximo na API V4: 10000

projects = {
    "GAV4": [{
        "cfg": [
            GaV4Config(
                page_size=PAGE_SIZE,
                profile_id=VIEW,
                request_body={
                    "reportRequests": [
                        {
                            "viewId": VIEW,
                            "samplingLevel": "LARGE",
                            "dimensions": [
                                {'name': 'ga:date'},
                                {'name': 'ga:field1'},
                                {'name': 'ga:field2'},
                                {'name': 'ga:field3'}
                            ],
                            "dimensionFilterClauses": [
                                {
                                    "filters": [
                                        {
                                            "dimensionName": "ga:dimension",
                                            "not": True,
                                            "operator": "EXACT",
                                            "expressions": [""]
                                        }
                                    ]
                                }
                            ],
                            "metrics": [
                                {'expression': 'ga:metric1'},
                                {'expression': 'ga:metric2'},
                                {'expression': 'ga:metric3'}
                            ],
                            "pageSize": PAGE_SIZE
                        }
                    ],
                }
            )
        ]
    }]
}

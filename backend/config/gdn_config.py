#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GdnConfig:
    def __init__(
            self,
            version,
            report_type,
            fields,
            date_range,
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.version = version
        self.report_type = report_type
        self.fields = fields
        self.date_range = date_range
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# GDN Report params
REPORT_VERSION = "v201802"  # update version https://developers.google.com/adwords/api/docs/reference/
DATE_RANGE = "LAST_7_DAYS"  # date_range useful values: LAST_7_DAYS, THIS_MONTH, LAST_MONTH, ALL_TIME, CUSTOM_DATE
AD_REPORT = "AD_PERFORMANCE_REPORT"
AD_GROUP_REPORT = "ADGROUP_PERFORMANCE_REPORT"
CAMPAIGN_REPORT = "CAMPAIGN_PERFORMANCE_REPORT"
KEYWORD_REPORT = "KEYWORDS_PERFORMANCE_REPORT"

# Client Custumer ID -> Relatorios que serao executados para cada id
GDN_IDS = [
    "1-1-1"
]

projects = {
    "GDN": [{
        "client_custumer_id": GDN_IDS,
        "cfg": [
            GdnConfig(  # All Info
                version=REPORT_VERSION,
                report_type=KEYWORD_REPORT,
                fields=[
                    'Date',
                    'AccountDescriptiveName',
                    'KeywordMatchType',
                    'CampaignId',
                    'CampaignName',
                    'AdGroupId',
                    'AdGroupName',
                    'Impressions',
                    'Clicks',
                    'Cost',
                    'AveragePosition',
                    'QualityScore',
                    'SearchImpressionShare',
                    'SearchRankLostImpressionShare'
                ],
                date_range=DATE_RANGE,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="table_name",
                    local_folder="csvlocation\\GDN\\ToLoad",
                    local_folder_loaded="csvlocation\\GDN\\Loaded",
                    local_folder_error="csvlocation\\GDN\\Error",
                    schema=[
                        {"name": "Date", "type": "string"},
                        {"name": "Account", "type": "string"},
                        {"name": "Keyword", "type": "string"},
                        {"name": "CampaignId", "type": "string"},
                        {"name": "Campaign", "type": "string"},
                        {"name": "AdGroupId", "type": "string"},
                        {"name": "AdGroup", "type": "string"},
                        {"name": "Impressions", "type": "string"},
                        {"name": "Clicks", "type": "string"},
                        {"name": "Cost", "type": "string"},
                        {"name": "AveragePosition", "type": "string"},
                        {"name": "QualityScore", "type": "string"},
                        {"name": "SearchImpressionShare", "type": "string"},
                        {"name": "SearchRankLostImpressionShare", "type": "string"}
                    ],
                    skip_rows=1
                )
            )
        ]
    }]
}

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class DcmConfig:
    def __init__(
            self,
            report_id,
            skip_rows_csv_process=0,
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.report_id = report_id
        self.skip_rows_csv_process = skip_rows_csv_process
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# Main profile dcm account
DCM_PROFILE_ID = 1

# Reports IDs
REPORT_ID_EXAMPLE = 1

projects = {
    "DCM": [{
        "profile_id": DCM_PROFILE_ID,
        "cfg": [
            DcmConfig(
                report_id=REPORT_ID_EXAMPLE,
                skip_rows_csv_process=14,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="table_name",
                    local_folder="csvlocation\\DCM\\folder\\ToLoad",
                    local_folder_loaded="csvlocation\\DCM\\folder\\Loaded",
                    local_folder_error="csvlocation\\DCM\\folder\\Error",
                    schema=[
                        {"name": "Date", "type": "TIMESTAMP"},
                        {"name": "field1", "type": "STRING"},
                        {"name": "field2", "type": "STRING"},
                        {"name": "field3", "type": "STRING"}
                    ],
                    skip_rows=0
                )
            )
        ]
    }]
}

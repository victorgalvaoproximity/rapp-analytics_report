#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class DbmConfig:
    def __init__(
            self,
            enabled=True,
            foot_size=0,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.enabled = enabled
        self.foot_size = foot_size
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# Reports IDs
REPORT_ID_EXAMPLE = 1

projects = {
    "DBM": [{
        "query_id": REPORT_ID_EXAMPLE,
        "cfg": [
            DbmConfig(
                foot_size=28,
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="table_name",
                    local_folder="csvlocation\\DBM\\ToLoad",
                    local_folder_loaded="csvlocation\\DBM\\Loaded",
                    local_folder_error="csvlocation\\DBM\\Error",
                    schema=[
                        {"name": "Date", "type": "string"},
                        {"name": "field1", "type": "string"},
                        {"name": "field2", "type": "string"},
                        {"name": "field3", "type": "string"}
                    ],
                    skip_rows=1,
                    limit_days=7
                )
            )
        ]
    }]
}

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GoogleSheetConfig:
    def __init__(
            self,
            spreadsheet_id,
            sheet_name,
            first_column_index,
            last_column_index,
            report_name="",
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.spreadsheet_id = spreadsheet_id
        self.sheet_name = sheet_name
        self.first_column_index = first_column_index
        self.last_column_index = last_column_index
        self.report_name = report_name
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# Google Spreadsheet IDs
API_ID = '1'

projects = {
    "GOOGLE_SHEET": [{
        "cfg": [
            GoogleSheetConfig(
                spreadsheet_id=API_ID,
                sheet_name="Data1",
                first_column_index="A",
                last_column_index="S",
                report_name="report_name",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="table_name",
                    local_folder="csvlocation\\Sheet\\ToLoad",
                    local_folder_loaded="csvlocation\\Sheet\\Loaded",
                    local_folder_error="csvlocation\\Sheet\\Error",
                    schema=[
                        {"name": "Date", "type": "STRING"},
                        {"name": "CampaignName", "type": "STRING"},
                        {"name": "CampaignID", "type": "STRING"},
                        {"name": "AdSetName", "type": "STRING"},
                        {"name": "AdSetID", "type": "STRING"},
                        {"name": "AdName", "type": "STRING"},
                        {"name": "AdID", "type": "STRING"},
                        {"name": "AmountSpent", "type": "STRING"},
                        {"name": "Impressions", "type": "STRING"},
                        {"name": "LinkClicks", "type": "STRING"},
                        {"name": "PostReactions", "type": "STRING"},
                        {"name": "PostShares", "type": "STRING"},
                        {"name": "PostComments", "type": "STRING"},
                        {"name": "ThreeSecondVideoViews", "type": "STRING"},
                        {"name": "30SecondVideoViews", "type": "STRING"},
                        {"name": "VideoWatchesAt25", "type": "STRING"},
                        {"name": "VideoWatchesAt50", "type": "STRING"},
                        {"name": "VideoWatchesAt75", "type": "STRING"},
                        {"name": "VideoWatchesAt100", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            )
        ]
    }]
}

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.main_config import *


class GaUnsampledConfig:
    def __init__(
            self,
            dimensions,
            metrics,
            goal_values="",
            filters="",
            report_name="",
            enabled=True,
            sql_server_load_config=SqlServerLoadConfig()
    ):
        self.dimensions = dimensions
        self.metrics = metrics
        self.goal_values = goal_values
        self.filters = filters
        self.report_name = report_name
        self.enabled = enabled
        self.database = sql_server_load_config.database
        self.table_file = sql_server_load_config.table_file
        self.local_folder = sql_server_load_config.local_folder
        self.local_folder_loaded = sql_server_load_config.local_folder_loaded
        self.local_folder_error = sql_server_load_config.local_folder_error
        self.schema = sql_server_load_config.schema
        self.skip_rows = sql_server_load_config.skip_rows
        self.limit_days = sql_server_load_config.limit_days
        self.code_page = sql_server_load_config.code_page


# GA
ACCOUNT = "1"
PROPERTY = "UA-1-1"
VIEW = "1"

projects = {
    "GA_UNSAMPLED": [{
        "account_summaries": [
            {
                "account_id": ACCOUNT,
                "web_property_id": PROPERTY,
                "profile_id": VIEW
            }
        ],
        "cfg": [
            GaUnsampledConfig(
                dimensions="ga:dimensions",
                metrics="ga:metrics",
                filters="",
                report_name="report_name",
                sql_server_load_config=SqlServerLoadConfig(
                    database=MSSQL_BI_ANALYTICS_REPORT,
                    table_file="table_name",
                    local_folder="csvlocation\\GA_Unsampled\\ToLoad",
                    local_folder_loaded="csvlocation\\GA_Unsampled\\Loaded",
                    local_folder_error="csvlocation\\GA_Unsampled\\Error",
                    schema=[
                        {"name": "date", "type": "STRING"},
                        {"name": "field1", "type": "STRING"},
                        {"name": "field2", "type": "STRING"},
                        {"name": "field3", "type": "STRING"}
                    ],
                    skip_rows=1
                )
            )
        ]
    }]
}

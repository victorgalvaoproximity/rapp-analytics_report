#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config.ga_unsampled_config as config
import csv
import logging
import logging.handlers
import os
import sys
import time
import traceback
import importlib

from authentication import Authentication
from datetime import datetime, date
from abstract_process import AbstractProcess
from sanitizer import Sanitizer


class GaUnsampledProcess(AbstractProcess):
    def __init__(self):
        super(GaUnsampledProcess, self).__init__("GA_UNSAMPLED")
        self.__setup_logger()

    def __setup_logger(self):
        """
        Inicializador de log para todo o processo de aquisição e upload de dados
        """
        log_path = "logs"
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        log_handler = logging.handlers.WatchedFileHandler(
            "logs/{0}_log_{1}.log".format(
                "GA_UNSAMPLED",
                date.today().strftime('%d%m%Y')
            )
        )
        formatter = logging.Formatter(
            '[%(levelname)s] %(asctime)s - %(name)s \n\t%(message)s',
            '%Y/%m/%d %H:%M:%S'
        )

        formatter.converter = time.gmtime
        log_handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.handlers = []
        logger.addHandler(log_handler)
        logger.setLevel(logging.DEBUG)

    def __setup(self):
        """
        Inicializa ambiente para executar o processo
        :type local_folder: str
        """
        self.__setup_logger()

        logging.debug("[GA UNSAMPLED] Reconfiguração de ambiente iniciada")
        stdin, stdout = sys.stdin, sys.stdout
        importlib.reload(sys)
        sys.stdin, sys.stdout = stdin, stdout
        logging.debug("[GA UNSAMPLED] Reconfiguração de ambiente finalizada")

    def __setup_folder(self, local_folder):
        """
        Cria diretorio para armazenar os CSV de acordo com o cliente
        :type local_folder: str
        """
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)

    def __find_in_array(self, array, key, value):
        return [x for x in array if x[key] == value]

    def __get_account_summaries(
            self,
            service,
            max_results
    ):
        """
        Retorna lista de contas do Google Analytics disponíveis para acesso
        :type service: Resource
        :type max_results: int
        """
        data_query = service.management().accountSummaries().list(**{
            u"max_results": int(max_results)
        })
        account_summaries = data_query.execute()
        return account_summaries

    def __ga_unsampled_data(
            self,
            account_id,
            web_property_id,
            profile_id,
            name,
            service,
            service_drive,
            metrics,
            dimensions,
            filters,
            template_filename,
            report_name,
            csv_path
    ):
        """
        Busca os dados no Google Analytics de acordo com as metrics e dimensions informadas,
         cria arquivo CSV com os dados recebidos
        :type account_id: str
        :type web_property_id: str
        :type profile_id: str
        :type name: str
        :type service: Resource
        :type service_drive: Resource
        :type metrics: str
        :type dimensions: str
        :type filters: str
        :type csv_path: str
        """

        report_of_day = None

        list_unsampled_reports = service.management().unsampledReports().list(
            accountId=account_id,
            webPropertyId=web_property_id,
            profileId=profile_id
        ).execute()

        for reports in list_unsampled_reports["items"]:
            date_report = datetime.strptime(reports["created"], '%Y-%m-%dT%H:%M:%S.%fZ').date()
            if date_report == datetime.now().date() and report_name in reports["title"]:
                report_of_day = reports["id"]
                print("Report {0} ja criado para o dia {1}".format(report_name, datetime.now().date()))

        if not report_of_day:
            request_unsampled = service.management().unsampledReports().insert(
                accountId=account_id,
                webPropertyId=web_property_id,
                profileId=profile_id,
                body={
                    "title": template_filename,
                    "metrics": metrics,
                    "dimensions": dimensions,
                    "filters": filters,
                    "start-date": "7daysAgo",
                    "end-date": "yesterday"
                }
            ).execute()
            report_of_day = request_unsampled[u"id"]
            print("Insert Report: {0}. Report ID: {1}".format(report_name, report_of_day))

        while True:
            get_report = service.management().unsampledReports().get(
                accountId=account_id,
                webPropertyId=web_property_id,
                profileId=profile_id,
                unsampledReportId=report_of_day
            ).execute()

            print("Status = {0}".format(get_report[u"status"]))

            if get_report[u"status"] == u'COMPLETED':
                drive_file_id = str(get_report[u"driveDownloadDetails"][u"documentId"])

                response_media = service_drive.files().get_media(
                    fileId=drive_file_id
                ).execute()

                logging.debug(
                    "Report recuperado com sucesso, iniciando criacao do arquivo CSV ({0})".format(template_filename)
                )

                response_media = response_media.decode("utf-8")
                response_media = response_media.replace(";", ".")
                response_media = Sanitizer.normalize_response_media(
                    response_media=response_media,
                    need_sql_adjustment=True
                )

                rollup_populate_report = None  # Especifico para reports sem data - VW ROLLUP (reports feitos a mao)

                # response_media = Sanitizer.normalize_unicode_data(response_media, "sql", limit_char=500)
                response_media[6] = "{0};accountId;webPropertyId;profileId;accountName;reportName".format(
                    response_media[6]
                )

                if rollup_populate_report:
                    response_media[6] = "{0};date;start_date;end_date".format(
                        response_media[6]
                    )

                for i in range(len(response_media)):
                    # response_media[i] = Sanitizer.clean_semicolon_between_quotes(response_media[i])
                    if i != 6:
                        response_media[i] = "{0};{1};{2};{3};{4};{5}".format(
                            response_media[i],
                            account_id,
                            web_property_id,
                            profile_id,
                            name,
                            report_name
                        )

                        if rollup_populate_report:
                            response_media[i] = "{0};{1};{2};{3}".format(
                                response_media[i],
                                "2018-10-08",
                                "2017-01-01",
                                "2018-09-30"
                            )

                with open(csv_path, 'w', newline='', encoding='utf-8') as csv_file:
                    csv_writer = csv.writer(
                        csv_file,
                        delimiter=";",
                        dialect="excel"
                    )
                    for rows in response_media[6:]:  # Ignoring the header and footer
                        row = rows.split(";")
                        for col in range(len(row)):
                            row[col] = row[col][:500]
                        csv_writer.writerow(row)
                csv_file.close()

                logging.debug("Escrita do arquivo CSV local finalizada, arquivo fechado")

                break
            elif get_report[u"status"] == u'FAILED':
                logging.debug("Erro durante aquisição de dados Unsampled. Dados do arquivo: {0}".format(get_report))
                break
            else:
                time.sleep(30)
        return "Done: {0}".format(report_name)

    def ga_unsampled_process(
            self,
            accounts_to_run,
            report_config
    ):
        """
        Inicia e coordena todo o processo que envolve aquisição de dados
        :type accounts_to_run: dict
        :type report_config: dict
        """
        self.__setup()

        service = Authentication().get_service_from_source("GA")
        service_drive = Authentication().get_service_from_source("DRIVE")

        account_summaries = self.__get_account_summaries(service, 10000)

        if "items" not in account_summaries:
            raise ValueError("O resultado obtido da API não contém a chave 'items'...")

        account_summary_items = account_summaries["items"]

        for item in accounts_to_run:
            account = self.__find_in_array(account_summary_items, "id", item["account_id"])
            goal_values = ""
            if "goal_values" in item:
                goal_values = item["goal_values"]

            web_property = []
            profile = []

            if len(account) > 0:
                web_properties = account[0]["webProperties"]
                web_property = self.__find_in_array(web_properties, "id", item["web_property_id"])

            if len(web_property) > 0:
                profiles = web_property[0]["profiles"]
                profile = self.__find_in_array(profiles, "id", item["profile_id"])

            if len(profile) > 0:
                first_account = account[0]
                first_web_property = web_property[0]
                first_profile = profile[0]
                first_account_name = Sanitizer.sanitize_string(str(first_account["name"]))

                for reports in filter(lambda r: r.enabled, report_config):
                    template_filename = reports.table_file
                    local_folder = reports.local_folder

                    logging.debug("Iniciando Report: {0}".format(template_filename))

                    logging.debug("Formatando o nome do arquivo local")

                    file_name = "{0}_{1}.csv".format(
                        template_filename,
                        datetime.now().strftime("%Y%m%d%H%M%S%f")
                    )

                    logging.debug("Formatação concluida: {0}".format(file_name))

                    logging.debug("Formatando caminho completo local")

                    csv_path = "{0}\\{1}".format(
                        os.getcwd(),
                        local_folder
                    )

                    self.__setup_folder(csv_path)

                    csv_path = "{0}\\{1}".format(
                        csv_path,
                        file_name
                    )

                    logging.debug("Formatação concluida: {0}".format(csv_path))

                    print(
                        self.__ga_unsampled_data(
                            account_id=first_account["id"],
                            web_property_id=first_web_property["id"],
                            profile_id=first_profile["id"],
                            name=first_account_name,
                            service=service,
                            service_drive=service_drive,
                            metrics=reports.metrics.replace("ga:goalX1Value,ga:goalX2Value", goal_values),
                            dimensions=reports.dimensions,
                            filters=reports.filters,
                            template_filename=template_filename,
                            report_name=reports.report_name,
                            csv_path=csv_path
                        )
                    )

    def acquisition(self):
        """
        Executa o processo de criação de arquivo CSV a partir de uma fonte de dados
        """
        process = GaUnsampledProcess()
        for report in config.projects["GA_UNSAMPLED"]:
            account_summaries = report["account_summaries"]
            report_config = report["cfg"]
            try:
                process.ga_unsampled_process(
                    account_summaries,
                    report_config
                )
            except Exception as ex:
                logging.exception(ex)

    def mssql_store(self, project=config.projects["GA_UNSAMPLED"], bulk_insert=True, field_terminator=";"):
        super(GaUnsampledProcess, self).mssql_store(project=project)


if __name__ == "__main__":
    process = GaUnsampledProcess()
    for report in config.projects["GA_UNSAMPLED"]:
        account_summaries = report["account_summaries"]
        report_config = report["cfg"]
        try:
            process.ga_unsampled_process(
                account_summaries,
                report_config
            )
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = ''.join('!! ' + line for line in lines)
            logging.exception(msg)
            print(msg)

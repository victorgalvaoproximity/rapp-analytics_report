#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pypyodbc

from datetime import datetime, timedelta
from authentication import sql_server as mssql_data
import unicodecsv as csv


class SqlServer:
    def __init__(self):
        pass

    def __mssql_type(self, og_type):
        """
        Adapta data types do bigquery pro Sql Server, permitindo usar o mesmo schema para as duas bases
        """
        new_type = og_type.replace('INTEGER', 'int').replace('integer', 'int') \
            .replace('STRING', 'varchar(500)').replace('string', 'varchar(500)') \
            .replace('TIMESTAMP', 'datetime').replace('timestamp', 'datetime') \
            .replace('BOOLEAN', 'bit').replace('boolean', 'bit')
        return new_type

    def __open_conn(self, database):
        """
        Abre conexao com banco de dados SQL Server
        """
        server = mssql_data["host"]
        user = mssql_data["user"]
        password = mssql_data["password"]
        connection_string = 'Driver={SQL Server};Server=' + server + ';Database=' + database + ';UID=' + user + ';PWD=' + password + ';'
        conn = pypyodbc.connect(connection_string)
        # conn = pymssql.connect(server, user, password, database, port=1433)
        return conn

    def __clean_recent(self, database, table, limit_days=7):
        """
        SQL de limpeza de dados que serao atualizados
        """
        conn = self.__open_conn(database)
        cursor = conn.cursor()
        check_table = self.check_existing_table(database, table)
        date_limit = (datetime.today() - timedelta(days=limit_days + 1)).date()
        if check_table is not None:
            query = "delete from {0} where cast([Date] as date) > '{1}'".format(table, date_limit)
            cursor.execute(query)
            conn.commit()

    def check_existing_table(self, database, table):
        """
        SQL para verificar se a tabela ja existe na base
        """
        conn = self.__open_conn(database)
        cursor = conn.cursor()
        query = "select object_id from sys.tables where type = 'U' and name = '{0}'".format(table)
        cursor.execute(query)
        object_id = cursor.fetchone()
        if object_id is not None:
            object_id = object_id[0]
        return object_id

    def create_table(self, database, table, schema):
        """
        SQL para criar a table de acordo com o schema, faz um drop se ela ja existir
        """
        conn = self.__open_conn(database)
        cursor = conn.cursor()
        query = "if object_id('{0}', 'U') is not null drop table {0} ".format(table)
        query += "create table {0} ( ".format(table)
        columns = ""
        for col in schema:
            if col["name"] == "Activities":
                columns += "[{0}] {1},".format(col["name"], "nvarchar(max)")
            else:
                columns += "[{0}] {1},".format(col["name"], self.__mssql_type(col["type"]))
        query += columns[:-1] + ")"
        cursor.execute(query)
        conn.commit()

    def load_file(self, database, table, file_path, skip_rows=0, limit_days=7,
                  code_page="65001", bulk_insert=True, field_terminator=";"):
        """
        SQL para carregar dados de arquivo CSV direto na table
        """
        conn = self.__open_conn(database)
        cursor = conn.cursor()
        # bulk_insert = False
        if bulk_insert:
            """
            Proc que controla a carga de dados extraidos para o SQL Server
            Atributo @dias_uteis_fechamento indica a partir de qual dia util serão desconsiderados dados do mês anterior
            """
            query = '''
                    exec sp_carrega_arquivo_extracao
                    @tabela = {0},
                    @path_arquivo = '{1}',
                    @linha_inicial = {2},
                    @dias_corte = {3},
                    @code_page = '{4}',
                    @field_terminator = '{5}'
            '''.format(
                table,
                file_path,
                skip_rows + 1,
                limit_days,
                code_page,
                field_terminator
            )
            cursor.execute(query)
            conn.commit()
        else:
            # if limit_days > 0:  # regra limitada para quando nao executa o bulk insert
                # self.__clean_recent(database, table, limit_days)
            with open(file_path, 'rb') as csv_file:
                reader = csv.DictReader(csv_file, delimiter=';')
                for row in reader:
                    placeholders = ', '.join(['?'] * len(row))
                    columns = ', '.join(row.keys())
                    columns = columns.replace('ga:', '')
                    query = "INSERT INTO %s ( %s ) VALUES ( %s )" % (table, columns, placeholders)
                    values = row.values()
                    cursor.execute(query, tuple(values))
            conn.commit()

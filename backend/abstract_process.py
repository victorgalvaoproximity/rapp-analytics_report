# coding=utf-8
import logging
import os

from abc import ABCMeta, abstractmethod
from sql_server import SqlServer


class AbstractProcess:
    __metaclass__ = ABCMeta

    def __init__(self, process_name):
        self.process = process_name

    @abstractmethod
    def acquisition(self):
        raise NotImplementedError()

    @abstractmethod
    def mssql_store(self, project, bulk_insert=True, field_terminator=";"):
        """
        Realiza a carga de dados dos arquivos CSV gerados direto para a base SQL Server

        :param project:  Configuracao geral do processo a ser executado
        :param bulk_insert: Frag de inserção no banco de dados.
        :param field_terminator: Delimitador do csv, padrao ponto e virgula
        """

        sql_process = SqlServer()

        for config in project:
            for report in filter(lambda r: r.enabled, config["cfg"]):  # filtra reports ativos
                local_report_created = []
                local_folder = report.local_folder
                table_file = report.table_file
                database = report.database

                for root, sub_folders, files in os.walk(local_folder):
                    for file in files:
                        local_report_created.append(
                            dict(path=root, file=file)
                        )

                local_files_to_store = []
                for local_report in local_report_created:
                    if table_file == local_report["file"][:-25]:  # file name without timestamp
                        local_files_to_store.append(local_report)

                logging.debug(
                    "Iniciando carga de dados para servidor SQL Server. Table: {0}; DB: {1}".format(
                        table_file,
                        database
                    )
                )

                check_table = sql_process.check_existing_table(database, table_file)
                # check_table = None
                if check_table is None:
                    schema = report.schema
                    sql_process.create_table(database, table_file, schema)

                file_count = 0
                for local_file_dict in local_files_to_store:
                    skip_rows = report.skip_rows
                    file_name = local_file_dict["file"]
                    local_file = "{0}\\{1}\\{2}".format(
                        os.getcwd(),
                        local_file_dict["path"],
                        file_name
                    )
                    limit_days = report.limit_days
                    code_page = report.code_page
                    if file_count > 0:
                        # Evita que o clean seja executado mais de uma vez para
                        # processos que carregam mais de um arquivo numa mesma tabela
                        limit_days = 0
                    try:
                        sql_process.load_file(database, table_file, local_file, skip_rows, limit_days, code_page,
                                              bulk_insert=bulk_insert, field_terminator=field_terminator)
                        # Loaded path
                        if not os.path.exists(report.local_folder_loaded):
                            os.makedirs(report.local_folder_loaded)
                        local_folder_loaded = "{0}\\{1}\\{2}".format(
                            os.getcwd(),
                            report.local_folder_loaded,
                            file_name
                        )
                        os.rename(local_file, local_folder_loaded)
                        file_count += 1
                        # os.remove(local_file)
                    except Exception as sql_error:
                        print (sql_error)
                        # Error path
                        if not os.path.exists(report.local_folder_error):
                            os.makedirs(report.local_folder_error)
                        local_folder_error = "{0}\\{1}\\{2}".format(
                            os.getcwd(),
                            report.local_folder_error,
                            file_name
                        )
                        os.rename(local_file, local_folder_error)
                        file_count += 1
                        logging.debug(sql_error)
